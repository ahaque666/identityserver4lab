﻿using System.Threading.Tasks;
using IdentityModel.Client;

namespace ClientMVC.Services
{
    public interface ITokenService
    {
        Task<TokenResponse> GetToken(string scope);
    }
}
